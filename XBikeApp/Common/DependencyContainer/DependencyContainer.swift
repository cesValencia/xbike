//
//  DependencyContainer.swift
//  XBikeApp
//
//  Created by David Valencia on 09/08/22.
//

import Foundation

class DependencyContainer {
    
    let localService: LocalManager
    let defaults: DefaultsManager
    
    init(localService: LocalManager = LocalManager(), defaults: DefaultsManager = DefaultsManager(defaults: UserDefaults.standard)) {
        self.localService = localService
        self.defaults = defaults
    }
}
