//
//  MainCoordinator.swift
//  XBikeApp
//
//  Created by David Valencia on 09/08/22.
//

import Foundation
import UIKit

typealias Factories = OnboardingFactory&PageFactory&TabBarFactory&CurrentRideFactory&MyProgressFactory

protocol MainCoordinatorProtocol: Coordinator {
    func openHome()
    func openOnboarding(_ controllers: [UIViewController])
}

class MainCoordinator: MainCoordinatorProtocol {

    var childCoordinators: [Coordinator] = [Coordinator]()
    var navigationController: UINavigationController
    private let factory: Factories
    
    init(factory: Factories, navigationController: UINavigationController) {
        self.factory = factory
        self.navigationController = navigationController
    }
    
    func openHome() {
        let currentRideView = self.factory.makeCurrentRideView(.white)
        let currentRideVC = self.factory.makeCurrentRideVC(currentRideView, coordinator: self)
        let currentRideItem = UITabBarItem(title: "Current ride", image: UIImage(named: "ride"), selectedImage: UIImage(named: "ride"))
        currentRideVC.tabBarItem = currentRideItem
        currentRideVC.title = "Current ride"
        
        let navRideAppereance = UINavigationBarAppearance()
        navRideAppereance.configureWithOpaqueBackground()
        navRideAppereance.backgroundColor = .systemOrange
        navRideAppereance.shadowColor = .clear
        navRideAppereance.shadowImage = UIImage()
        
        let rideNav = UINavigationController(rootViewController: currentRideVC)
        rideNav.navigationBar.standardAppearance = navRideAppereance
        rideNav.navigationBar.scrollEdgeAppearance = navRideAppereance
        rideNav.navigationBar.tintColor = .white
        
        let myProgressView = self.factory.makeMyProgressView(.white)
        let myProgressVC = self.factory.makeMyProgressVC(myProgressView, coordinator: self)
        let myProgressItem = UITabBarItem(title: "My progress", image: UIImage(named: "progress"), selectedImage: UIImage(named: "progress"))
        myProgressVC.tabBarItem = myProgressItem
        myProgressVC.title = "My progress"
        
        let navProgressAppereance = UINavigationBarAppearance()
        navProgressAppereance.configureWithOpaqueBackground()
        navProgressAppereance.backgroundColor = .systemOrange
        navProgressAppereance.shadowColor = .clear
        navProgressAppereance.shadowImage = UIImage()
        
        let progressNav = UINavigationController(rootViewController: myProgressVC)
        progressNav.navigationBar.standardAppearance = navProgressAppereance
        progressNav.navigationBar.scrollEdgeAppearance = navProgressAppereance
        progressNav.navigationBar.tintColor = .white
        
        let vcs = [rideNav, progressNav]
        
        let tabVC = self.factory.makeTabBarVC(self, controllers: vcs)
        
        self.navigationController.setViewControllers([tabVC], animated: true)
    }
    
    func openOnboarding(_ controllers: [UIViewController]) {
        let vc = self.factory.makeOnboardingVC(self, controllers: controllers)
        
        self.navigationController.setViewControllers([vc], animated: false)
    }
    
    func start() {
        
        let pagesData = [
            PageData(iconName: "t1", descriptionText: "Extremely simple to use"),
            PageData(iconName: "t2", descriptionText: "Track your time and\ndistance"),
            PageData(iconName: "t3", descriptionText: "See yout progress\nand challenge\nyourself!")
        ]
        
        var controllers: [UIViewController] = [UIViewController]()
        
        pagesData.forEach { item in
            let view = self.factory.makePageView(.systemOrange)
            let vc = self.factory.makePageVC(view, coordinator: self, data: item)
            
            controllers.append(vc)
        }
        
        self.openOnboarding(controllers)
    }

}
