//
//  Coordinator.swift
//  XBikeApp
//
//  Created by David Valencia on 09/08/22.
//

import Foundation
import UIKit

protocol Coordinator: AnyObject {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }

    func start()
}

