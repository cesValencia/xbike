//
//  LocalManager.swift
//  XBikeApp
//
//  Created by David Valencia on 09/08/22.
//

import Foundation
import CoreData

enum LocalError: String, Swift.Error {
    case decodingError = "Error al decodificar el objeto"
}

protocol LocalService {
    var persistentContainer: NSPersistentContainer { get }
}

class LocalManager: LocalService {
    
    var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "XBikeApp")
        container.loadPersistentStores { (storeDescription, err) in
            if let err = err {
                fatalError("Loading of store failed: \(err)")
            }
        }
        return container
    }()
    
}
