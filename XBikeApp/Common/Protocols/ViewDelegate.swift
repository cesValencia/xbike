//
//  ViewDelegate.swift
//  XBikeApp
//
//  Created by David Valencia on 09/08/22.
//

import Foundation

protocol ViewDelegate {
    func initComponents()
    func addComponents()
    func setAutolayout()
    func setDelegates()
}

extension ViewDelegate {
    func setDelegates() {}
}
