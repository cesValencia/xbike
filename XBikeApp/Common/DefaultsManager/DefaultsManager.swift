//
//  DefaultsManager.swift
//  XBikeApp
//
//  Created by David Valencia on 09/08/22.
//

import Foundation

protocol DefaultsManagerDelegate {
    var defaults: UserDefaults { get set }
    
    init(defaults: UserDefaults)
    
    func save(_ value: Any?, key: String)
    func delete(_ key: String)
    func getValue(_ key: String) -> Any?
}

class DefaultsManager: DefaultsManagerDelegate {
    
    var defaults: UserDefaults
    
    required init(defaults: UserDefaults) {
        self.defaults = defaults
    }
    
    func save(_ value: Any?, key: String) {
        defaults.set(value, forKey: key)
    }
    
    func delete(_ key: String) {
        defaults.removeObject(forKey: key)
    }
    
    func getValue(_ key: String) -> Any? {
        defaults.value(forKey: key)
    }
    
}
