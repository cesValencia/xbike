//
//  Repository.swift
//  XBikeApp
//
//  Created by David Valencia on 09/08/22.
//

import Foundation

// MARK: - Base Repository

protocol Repository {
    init(localService: LocalManager)
}

// MARK: - Local Repository

protocol LocalRepository: Repository {
    
    var localService: LocalManager { get set }
    
}
