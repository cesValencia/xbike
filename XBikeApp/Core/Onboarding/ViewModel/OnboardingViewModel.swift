//
//  OnboardingViewModel.swift
//  XBikeApp
//
//  Created by David Valencia on 09/08/22.
//

import Foundation

protocol OnboardingViewProtocol {

}

protocol OnboardingViewModelProtocol {
}

class OnboardingViewModel: OnboardingViewModelProtocol {
    
    var view: OnboardingViewProtocol
    var repository: OnboardingRepositoryProtocol
    
    required init(view: OnboardingViewProtocol, repository: OnboardingRepositoryProtocol) {
        self.view = view
        self.repository = repository
    }

}
