//
//  OnboardingVC.swift
//  XBikeApp
//
//  Created by David Valencia on 09/08/22.
//

import UIKit

class OnboardingVC: UIPageViewController {
    
    lazy var pageControl: UIPageControl = {
        let control = UIPageControl()
        control.currentPageIndicatorTintColor = .white
        control.pageIndicatorTintColor = UIColor.lightGray
        control.currentPage = 0
        control.translatesAutoresizingMaskIntoConstraints = false
        
        return control
    }()
    
    var controllers: [UIViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view.
        self.view.backgroundColor = .systemOrange
        self.title = ""
        self.setDelegates()
        self.setControllers()
        self.configPageControl()

    }

    // MARK: - Controller's life cycle methods

    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Memory warning!")
    }

}

extension OnboardingVC {
    
    // MARK: - Page control configuration
    
    func setDelegates() {
        self.dataSource = self
        self.delegate = self
    }
    
    func setControllers() {
        setViewControllers([controllers[0]], direction: .forward, animated: true, completion: nil)
    }
    
    func configPageControl() {
        
        self.pageControl.numberOfPages = self.controllers.count
        self.view.addSubview(self.pageControl)
        
        NSLayoutConstraint.activate([
            self.pageControl.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0),
            self.pageControl.widthAnchor.constraint(equalTo: self.view.widthAnchor, constant: 20),
            self.pageControl.heightAnchor.constraint(equalToConstant: 20),
            self.pageControl.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        ])

    }
    
}

extension OnboardingVC: UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if let viewControllerIndex = self.controllers.firstIndex(of: viewController) {
            if viewControllerIndex != 0 {
                return self.controllers[viewControllerIndex - 1]
            }
        }
        return nil
    }
        
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if let viewControllerIndex = self.controllers.firstIndex(of: viewController) {
            if viewControllerIndex < self.controllers.count - 1 {
                
                return self.controllers[viewControllerIndex + 1]
            }
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        
        if let viewControllers = pageViewController.viewControllers {
            if let viewControllerIndex = self.controllers.firstIndex(of: viewControllers[0]) {
                self.pageControl.currentPage = viewControllerIndex
            }
        }
    }
    
}

extension OnboardingVC: OnboardingViewProtocol {
    
    
}
