//
//  OnboardingFactory.swift
//  XBikeApp
//
//  Created by David Valencia on 09/08/22.
//

import Foundation
import UIKit

protocol OnboardingFactory {
    func makeMainCoordinator(navigationController: UINavigationController) -> MainCoordinator
    func makeOnboardingViewModel(_ view: OnboardingViewProtocol, repository: OnboardingRepositoryProtocol) -> OnboardingViewModelProtocol
    func makeOnboardingRepository() -> OnboardingRepository
    func makeOnboardingVC(_ coordinator: MainCoordinator, controllers: [UIViewController]) -> OnboardingVC
}

extension DependencyContainer: OnboardingFactory {
    
    func makeMainCoordinator(navigationController: UINavigationController) -> MainCoordinator {
        
        let mainCoordinator = MainCoordinator(factory: self, navigationController: navigationController)
        
        return mainCoordinator
    }
    
    func makeOnboardingViewModel(_ view: OnboardingViewProtocol, repository: OnboardingRepositoryProtocol) -> OnboardingViewModelProtocol {
        
        let presenter = OnboardingViewModel(view: view, repository: repository)
        
        return presenter
    }
    
    func makeOnboardingRepository() -> OnboardingRepository {
        return OnboardingRepository(localService: localService)
    }

    func makeOnboardingVC(_ coordinator: MainCoordinator, controllers: [UIViewController]) -> OnboardingVC {
        
        let vc = OnboardingVC(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        vc.controllers = controllers
        
        return vc
    }

}

