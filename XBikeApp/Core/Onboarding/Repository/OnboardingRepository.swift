//
//  OnboardingRepository.swift
//  XBikeApp
//
//  Created by David Valencia on 09/08/22.
//

import Foundation

protocol OnboardingRepositoryProtocol: LocalRepository {
}

class OnboardingRepository: OnboardingRepositoryProtocol {
    
    var localService: LocalManager
    
    required init(localService: LocalManager) {
        self.localService = localService
    }

}
