//
//  MyProgressFactory.swift
//  XBikeApp
//
//  Created by David Valencia on 10/08/22.
//

import Foundation
import UIKit

protocol MyProgressFactory {
    func makeMyProgressViewModel(_ view: MyProgressViewProtocol, repository: MyProgressRepositoryProtocol) -> MyProgressViewModelProtocol
    func makeMyProgressRepository() -> MyProgressRepository
    func makeMyProgressView(_ bgColor: UIColor) -> MyProgressView
    func makeMyProgressVC(_ view: MyProgressView, coordinator: MainCoordinator) -> MyProgressVC
}

extension DependencyContainer: MyProgressFactory {
    
    func makeMyProgressViewModel(_ view: MyProgressViewProtocol, repository: MyProgressRepositoryProtocol) -> MyProgressViewModelProtocol {
        
        let ViewModel = MyProgressViewModel(view: view, repository: repository)
        
        return ViewModel
    }
    
    func makeMyProgressRepository() -> MyProgressRepository {
        return MyProgressRepository(localService: localService)
    }
    
    func makeMyProgressView(_ bgColor: UIColor) -> MyProgressView {
        let view = MyProgressView()
        view.backgroundColor = bgColor
        
        return view
    }
    
    func makeMyProgressVC(_ view: MyProgressView, coordinator: MainCoordinator) -> MyProgressVC {
        let vc = MyProgressVC(UI: view, factory: self, coordinator: coordinator, defaults: defaults)
        
        return vc
    }

}
