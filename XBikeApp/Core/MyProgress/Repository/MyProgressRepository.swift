//
//  MyProgressRepository.swift
//  XBikeApp
//
//  Created by David Valencia on 10/08/22.
//

import Foundation

protocol MyProgressRepositoryProtocol: LocalRepository {
}

class MyProgressRepository: MyProgressRepositoryProtocol {
    
    var localService: LocalManager
    
    required init(localService: LocalManager) {
        self.localService = localService
    }

}
