//
//  MyProgressViewModel.swift
//  XBikeApp
//
//  Created by David Valencia on 10/08/22.
//

import Foundation

protocol MyProgressViewProtocol {
    func showErrorAlert(_ message: String?)
}

protocol MyProgressViewModelProtocol {
}

class MyProgressViewModel: MyProgressViewModelProtocol {
    
    var view: MyProgressViewProtocol
    var repository: MyProgressRepositoryProtocol
    
    required init(view: MyProgressViewProtocol, repository: MyProgressRepositoryProtocol) {
        self.view = view
        self.repository = repository
    }

}
