//
//  MyProgressView.swift
//  XBikeApp
//
//  Created by David Valencia on 10/08/22.
//

import UIKit

protocol MyProgressViewDelegate: NSObjectProtocol {

}

class MyProgressView: UIView, ViewDelegate {

    
    weak var delegate: MyProgressViewDelegate?

    lazy var table: UITableView = {
        let table = UITableView()
        table.register(ProgressCell.self, forCellReuseIdentifier: ProgressCell.reuseId)
        table.translatesAutoresizingMaskIntoConstraints = false
        
        return table
    }()
    
    override func layoutSubviews() {}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.initComponents()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initComponents() {
        self.addComponents()
        self.setAutolayout()
        self.setDelegates()
    }
    
    func addComponents() {
        self.addSubview(table)
    }
    
    func setAutolayout() {
        NSLayoutConstraint.activate([
            self.table.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            self.table.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.table.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor),
            self.table.leadingAnchor.constraint(equalTo: self.leadingAnchor)
        ])
    }
    
    func setDelegates() {
        self.table.delegate = self
        self.table.dataSource = self
    }

}

extension MyProgressView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProgressCell.reuseId, for: indexPath) as? ProgressCell else {
            return UITableViewCell()
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

}
