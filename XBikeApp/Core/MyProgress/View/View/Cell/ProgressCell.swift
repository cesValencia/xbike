//
//  ProgressCell.swift
//  XBikeApp
//
//  Created by David Valencia on 10/08/22.
//

import UIKit

class ProgressCell: UITableViewCell, ViewDelegate {
    
    static let reuseId: String = "progress-reuse-id"
    
    lazy var timeLabel: UILabel = {
        let label = UILabel()
        label.text = "00:00:00"
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.textColor = UIColor.black
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    lazy var startAddressLabel: UILabel = {
        let label = UILabel()
        label.text = "A: QUERÉTARO"
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.textColor = UIColor.gray
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    lazy var endAddressLabel: UILabel = {
        let label = UILabel()
        label.text = "A: CDMX"
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.textColor = UIColor.gray
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    lazy var addressStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fill
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        return stack
    }()
    
    lazy var kmLabel: UILabel = {
        let label = UILabel()
        label.text = "10.0 km"
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.textColor = UIColor.black
        label.numberOfLines = 0
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.initComponents()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initComponents() {
        self.addComponents()
        self.setAutolayout()
    }
    
    func addComponents() {
        self.contentView.addSubview(timeLabel)
        self.addressStack.addArrangedSubview(startAddressLabel)
        self.addressStack.addArrangedSubview(endAddressLabel)
        self.contentView.addSubview(addressStack)
        self.contentView.addSubview(kmLabel)
    }
    
    func setAutolayout() {
        NSLayoutConstraint.activate([
            self.timeLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 20),
            self.timeLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -20),
            self.timeLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 20),
            
            self.kmLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 20),
            self.kmLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            self.kmLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -20),
            
            self.addressStack.topAnchor.constraint(equalTo: self.timeLabel.bottomAnchor, constant: 10),
            self.addressStack.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 20),
            self.addressStack.trailingAnchor.constraint(equalTo: self.kmLabel.leadingAnchor, constant: -20),
            self.addressStack.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -20),
        ])
    }
    
}
