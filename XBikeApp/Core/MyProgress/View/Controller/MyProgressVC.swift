//
//  MyProgressVC.swift
//  XBikeApp
//
//  Created by David Valencia on 10/08/22.
//

import UIKit

class MyProgressVC: UIViewController {
    
    let coordinator: MainCoordinator
    let UI: MyProgressView
    let defaults: DefaultsManager
    private var factory: MyProgressFactory
    lazy var ViewModel: MyProgressViewModelProtocol = {
        factory.makeMyProgressViewModel(self, repository: factory.makeMyProgressRepository())
    }()
    
    required init(UI: MyProgressView, factory: MyProgressFactory, coordinator: MainCoordinator, defaults: DefaultsManager) {
        self.UI = UI
        self.factory = factory
        self.coordinator = coordinator
        self.defaults = defaults
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = UI
    }
    
    override func viewDidLayoutSubviews() {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view.
        self.setDelegates()

    }

    // MARK: - Controller's life cycle methods

    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Memory warning!")
    }

}

extension MyProgressVC {
    
    // MARK: - Controller's methods
    
    private func setDelegates() {
        self.UI.delegate = self
    }
    
}

extension MyProgressVC: MyProgressViewDelegate {
    

    
}

extension MyProgressVC: MyProgressViewProtocol {
    
    func showErrorAlert(_ message: String?) {
        
    }
    
}
