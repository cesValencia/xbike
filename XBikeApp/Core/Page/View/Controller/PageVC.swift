//
//  PageVC.swift
//  XBikeApp
//
//  Created by David Valencia on 09/08/22.
//

import UIKit

class PageVC: UIViewController {
    
    let data: PageData
    let coordinator: MainCoordinator
    let UI: PageView
    let defaults: DefaultsManager
    private var factory: PageFactory
    lazy var ViewModel: PageViewModelProtocol = {
        factory.makePageViewModel(self, repository: factory.makePageRepository())
    }()
    
    required init(UI: PageView, factory: PageFactory, coordinator: MainCoordinator, defaults: DefaultsManager, data: PageData) {
        self.UI = UI
        self.factory = factory
        self.coordinator = coordinator
        self.defaults = defaults
        self.data = data
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = UI
    }
    
    override func viewDidLayoutSubviews() {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view.
        self.setDelegates()
        self.UI.model = self.data

    }

    // MARK: - Controller's life cycle methods

    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Memory warning!")
    }

}

extension PageVC {
    
    // MARK: - Controller's methods
    
    private func setDelegates() {
        self.UI.delegate = self
    }
    
}

extension PageVC: PageViewDelegate {
    
    func didSkipButtonTap() {
        self.coordinator.openHome()
    }
    
}

extension PageVC: PageViewProtocol {
    
    func showErrorAlert(_ message: String?) {
        
    }
    
}
