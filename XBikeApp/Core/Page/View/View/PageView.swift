//
//  PageView.swift
//  XBikeApp
//
//  Created by David Valencia on 09/08/22.
//

import UIKit

protocol PageViewDelegate: NSObjectProtocol {
    func didSkipButtonTap()
}

class PageView: UIView, ViewDelegate {
    
    var model: PageData? {
        didSet {
            if let data = model {
                self.icon.image = UIImage(named: data.iconName)
                self.descriptionLabel.text = data.descriptionText
            }
        }
    }
    
    weak var delegate: PageViewDelegate?
    
    lazy var icon: UIImageView = {
        let icon = UIImageView()
        icon.image = UIImage(named: "")
        icon.contentMode = .scaleAspectFit
        icon.translatesAutoresizingMaskIntoConstraints = false
        
        return icon
    }()
    
    lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "Extremely simple to use"
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    lazy var skipButton: UIButton = {
        let button = UIButton()
        button.setTitle("Skip", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.addTarget(self, action: #selector(didSkipButtonPress), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    override func layoutSubviews() {}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.initComponents()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initComponents() {
        self.addComponents()
        self.setAutolayout()
        self.setDelegates()
    }
    
    func addComponents() {
        self.addSubview(icon)
        self.addSubview(descriptionLabel)
        self.addSubview(skipButton)
    }
    
    func setAutolayout() {
        NSLayoutConstraint.activate([
            self.icon.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            self.icon.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 30),
            self.icon.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -30),
            
            self.descriptionLabel.topAnchor.constraint(equalTo: self.icon.bottomAnchor, constant: 20),
            self.descriptionLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            self.descriptionLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            
            self.skipButton.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -20),
            self.skipButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20)
        ])
    }

}

extension PageView {

    @objc
    private func didSkipButtonPress() {
        self.delegate?.didSkipButtonTap()
    }
    
}
