//
//  PageData.swift
//  XBikeApp
//
//  Created by David Valencia on 10/08/22.
//

import Foundation

struct PageData {
    
    let iconName: String
    let descriptionText: String
    
}
