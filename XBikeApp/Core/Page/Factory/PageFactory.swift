//
//  PageFactory.swift
//  XBikeApp
//
//  Created by David Valencia on 09/08/22.
//

import Foundation
import UIKit

protocol PageFactory {
    func makePageViewModel(_ view: PageViewProtocol, repository: PageRepositoryProtocol) -> PageViewModelProtocol
    func makePageRepository() -> PageRepository
    func makePageView(_ bgColor: UIColor) -> PageView
    func makePageVC(_ view: PageView, coordinator: MainCoordinator, data: PageData) -> PageVC
}

extension DependencyContainer: PageFactory {
    
    func makePageViewModel(_ view: PageViewProtocol, repository: PageRepositoryProtocol) -> PageViewModelProtocol {
        
        let ViewModel = PageViewModel(view: view, repository: repository)
        
        return ViewModel
    }
    
    func makePageRepository() -> PageRepository {
        return PageRepository(localService: localService)
    }
    
    func makePageView(_ bgColor: UIColor) -> PageView {
        let view = PageView()
        view.backgroundColor = bgColor
        
        return view
    }
    
    func makePageVC(_ view: PageView, coordinator: MainCoordinator, data: PageData) -> PageVC {
        let vc = PageVC(UI: view, factory: self, coordinator: coordinator, defaults: defaults, data: data)
        
        return vc
    }

}
