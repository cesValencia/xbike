//
//  PageViewModel.swift
//  XBikeApp
//
//  Created by David Valencia on 09/08/22.
//

import Foundation

protocol PageViewProtocol {
    func showErrorAlert(_ message: String?)
}

protocol PageViewModelProtocol {
}

class PageViewModel: PageViewModelProtocol {
    
    var view: PageViewProtocol
    var repository: PageRepositoryProtocol
    
    required init(view: PageViewProtocol, repository: PageRepositoryProtocol) {
        self.view = view
        self.repository = repository
    }

}
