//
//  PageRepository.swift
//  XBikeApp
//
//  Created by David Valencia on 09/08/22.
//

import Foundation

protocol PageRepositoryProtocol: LocalRepository {
}

class PageRepository: PageRepositoryProtocol {
    
    var localService: LocalManager
    
    required init(localService: LocalManager) {
        self.localService = localService
    }

}
