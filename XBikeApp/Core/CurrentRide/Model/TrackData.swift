//
//  TrackData.swift
//  XBikeApp
//
//  Created by David Valencia on 10/08/22.
//

import Foundation

struct TrackData {
    
    let time: String
    let km: String
    let startAddress: String
    let endAddress: String
    
}
