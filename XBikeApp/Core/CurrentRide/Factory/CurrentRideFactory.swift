//
//  CurrentRideFactory.swift
//  XBikeApp
//
//  Created by David Valencia on 10/08/22.
//

import Foundation
import UIKit

protocol CurrentRideFactory {
    func makeCurrentRideViewModel(_ view: CurrentRideViewProtocol, repository: CurrentRideRepositoryProtocol) -> CurrentRideViewModelProtocol
    func makeCurrentRideRepository() -> CurrentRideRepository
    func makeCurrentRideView(_ bgColor: UIColor) -> CurrentRideView
    func makeCurrentRideVC(_ view: CurrentRideView, coordinator: MainCoordinator) -> CurrentRideVC
}

extension DependencyContainer: CurrentRideFactory {
    
    func makeCurrentRideViewModel(_ view: CurrentRideViewProtocol, repository: CurrentRideRepositoryProtocol) -> CurrentRideViewModelProtocol {
        
        let ViewModel = CurrentRideViewModel(view: view, repository: repository)
        
        return ViewModel
    }
    
    func makeCurrentRideRepository() -> CurrentRideRepository {
        return CurrentRideRepository(localService: localService)
    }
    
    func makeCurrentRideView(_ bgColor: UIColor) -> CurrentRideView {
        let view = CurrentRideView()
        view.backgroundColor = bgColor
        
        return view
    }
    
    func makeCurrentRideVC(_ view: CurrentRideView, coordinator: MainCoordinator) -> CurrentRideVC {
        let vc = CurrentRideVC(UI: view, factory: self, coordinator: coordinator, defaults: defaults)
        
        return vc
    }

}
