//
//  CurrentRideViewModel.swift
//  XBikeApp
//
//  Created by David Valencia on 10/08/22.
//

import Foundation

protocol CurrentRideViewProtocol {
    func showErrorAlert(_ message: String?)
}

protocol CurrentRideViewModelProtocol {
    func saveTrack(_ data: TrackData) -> Bool
}

class CurrentRideViewModel: CurrentRideViewModelProtocol {
    
    var view: CurrentRideViewProtocol
    var repository: CurrentRideRepositoryProtocol
    
    required init(view: CurrentRideViewProtocol, repository: CurrentRideRepositoryProtocol) {
        self.view = view
        self.repository = repository
    }
    
    func saveTrack(_ data: TrackData) -> Bool {
        if self.repository.saveTrackData(data) {
            return true
        } else {
            return false
        }
    }

}
