//
//  CurrentRideView.swift
//  XBikeApp
//
//  Created by David Valencia on 10/08/22.
//

import UIKit
import GoogleMaps

protocol CurrentRideViewDelegate: NSObjectProtocol {

}

class CurrentRideView: UIView, ViewDelegate {

    
    weak var delegate: CurrentRideViewDelegate?

    lazy var map: GMSMapView = {
        let map = GMSMapView()
        map.isMyLocationEnabled = true
        map.settings.myLocationButton = true
        map.settings.compassButton = true
        map.translatesAutoresizingMaskIntoConstraints = false
        
        return map
    }()
    
    lazy var container: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 20.0
        view.isHidden = true
        view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view.layer.shadowRadius = 5.0
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.5
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    override func layoutSubviews() {}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.initComponents()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initComponents() {
        self.addComponents()
        self.setAutolayout()
        self.setDelegates()
    }
    
    func addComponents() {
        self.addSubview(map)
        self.addSubview(container)
    }
    
    func setAutolayout() {
        NSLayoutConstraint.activate([
            self.map.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            self.map.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.map.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor),
            self.map.leadingAnchor.constraint(equalTo: self.leadingAnchor),
        ])
    }

}

extension CurrentRideView {


}
