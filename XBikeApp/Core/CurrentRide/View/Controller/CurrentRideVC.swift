//
//  CurrentRideVC.swift
//  XBikeApp
//
//  Created by David Valencia on 10/08/22.
//

import UIKit
import CoreLocation
import GoogleMaps

class CurrentRideVC: UIViewController {
    
    var locationManager: CLLocationManager?
    
    let coordinator: MainCoordinator
    let UI: CurrentRideView
    let defaults: DefaultsManager
    private var factory: CurrentRideFactory
    lazy var ViewModel: CurrentRideViewModelProtocol = {
        factory.makeCurrentRideViewModel(self, repository: factory.makeCurrentRideRepository())
    }()
    
    required init(UI: CurrentRideView, factory: CurrentRideFactory, coordinator: MainCoordinator, defaults: DefaultsManager) {
        self.UI = UI
        self.factory = factory
        self.coordinator = coordinator
        self.defaults = defaults
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = UI
    }
    
    override func viewDidLayoutSubviews() {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view.
        self.setDelegates()
        self.setupNavigationBar()

    }

    // MARK: - Controller's life cycle methods

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setLocationManager()
    }
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Memory warning!")
    }

}

extension CurrentRideVC {
    
    // MARK: - Controller's methods
    
    func setLocationManager() {
        self.locationManager = CLLocationManager()
        self.locationManager?.delegate = self
        self.locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager?.requestWhenInUseAuthorization()
    }
    
    private func setDelegates() {
        self.UI.delegate = self
    }
    
    private func setupNavigationBar() {
        self.navigationItem.title = "Current ride"
        
        let addItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(didAddButtonTap))
        
        self.navigationItem.rightBarButtonItem = addItem
    }
    
    @objc
    private func didAddButtonTap() {
        print("ADD")
    }
    
}

extension CurrentRideVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location: CLLocation = locations.last {
            let zoomLevel = self.locationManager?.accuracyAuthorization == .fullAccuracy ? 15.0 : 10.0
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                                  longitude: location.coordinate.longitude,
                                                  zoom: Float(zoomLevel))
            
            self.UI.map.camera = camera
            self.UI.map.animate(to: camera)
            
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            marker.title = "César"
            marker.snippet = "Location"
            marker.map = self.UI.map
        }

    }
    

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {

      switch status {
      case .restricted:
        print("Restringido")
      case .denied:
        print("Denegado")
      case .notDetermined:
        print("Indefinido")
      case .authorizedAlways:
          self.locationManager?.startUpdatingLocation()
      case .authorizedWhenInUse:
          self.locationManager?.startUpdatingLocation()
      default:
        fatalError()
      }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.locationManager?.stopUpdatingLocation()
        print("Error: \(error)")
    }
    
    

    
}

extension CurrentRideVC: CurrentRideViewDelegate {
    

    
}

extension CurrentRideVC: CurrentRideViewProtocol {
    
    func showErrorAlert(_ message: String?) {
        
    }
    
}
