//
//  CurrentRideRepository.swift
//  XBikeApp
//
//  Created by David Valencia on 10/08/22.
//

import Foundation
import CoreData

protocol CurrentRideRepositoryProtocol: LocalRepository {
    func saveTrackData(_ parameters: TrackData) -> Bool
}

class CurrentRideRepository: CurrentRideRepositoryProtocol {
    
    var localService: LocalManager
    
    required init(localService: LocalManager) {
        self.localService = localService
    }
    
    func saveTrackData(_ parameters: TrackData) -> Bool {
        let context = self.localService.persistentContainer.viewContext

        let newEntity = NSEntityDescription.insertNewObject(forEntityName: "RideEntity", into: context) as? RideEntity

        newEntity?.time = parameters.time
        newEntity?.km = parameters.km
        newEntity?.startAddress = parameters.startAddress
        newEntity?.endAddress = parameters.endAddress

        do {
            try context.save()
            return true
        } catch let err {
            debugPrint(err.localizedDescription)
            return false
        }

    }

}
