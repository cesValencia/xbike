//
//  TabBarViewModel.swift
//  XBikeApp
//
//  Created by David Valencia on 10/08/22.
//

import Foundation

protocol TabBarViewProtocol {
    func showErrorAlert(_ message: String?)
}

protocol TabBarViewModelProtocol {
}

class TabBarViewModel: TabBarViewModelProtocol {
    
    var view: TabBarViewProtocol
    var repository: TabBarRepositoryProtocol
    
    required init(view: TabBarViewProtocol, repository: TabBarRepositoryProtocol) {
        self.view = view
        self.repository = repository
    }

}
