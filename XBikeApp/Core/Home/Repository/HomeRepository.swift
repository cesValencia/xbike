//
//  TabBarRepository.swift
//  XBikeApp
//
//  Created by David Valencia on 10/08/22.
//

import Foundation

protocol TabBarRepositoryProtocol: LocalRepository {
}

class TabBarRepository: TabBarRepositoryProtocol {
    
    var localService: LocalManager
    
    required init(localService: LocalManager) {
        self.localService = localService
    }

}
