//
//  HomeFactory.swift
//  XBikeApp
//
//  Created by David Valencia on 10/08/22.
//

import Foundation
import UIKit

protocol TabBarFactory {
    func makeTabBarViewModel(_ view: TabBarViewProtocol, repository: TabBarRepositoryProtocol) -> TabBarViewModelProtocol
    func makeTabBarRepository() -> TabBarRepository
    func makeTabBarVC(_ coordinator: MainCoordinator, controllers: [UIViewController]) -> TabBarVC
}

extension DependencyContainer: TabBarFactory {
    
    func makeTabBarViewModel(_ view: TabBarViewProtocol, repository: TabBarRepositoryProtocol) -> TabBarViewModelProtocol {
        
        let viewModel = TabBarViewModel(view: view, repository: repository)
        
        return viewModel
    }
    
    func makeTabBarRepository() -> TabBarRepository {
        return TabBarRepository(localService: localService)
    }
    
    func makeTabBarVC(_ coordinator: MainCoordinator, controllers: [UIViewController]) -> TabBarVC {
        let vc = TabBarVC(factory: self, coordinator: coordinator, defaults: defaults, controllers: controllers)
        
        return vc
    }

}
