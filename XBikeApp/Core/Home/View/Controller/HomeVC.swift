//
//  HomeVC.swift
//  XBikeApp
//
//  Created by David Valencia on 10/08/22.
//

import UIKit

class TabBarVC: UITabBarController {
    
    let controllers: [UIViewController]
    let coordinator: MainCoordinator
    let defaults: DefaultsManager
    private var factory: TabBarFactory
    lazy var ViewModel: TabBarViewModelProtocol = {
        factory.makeTabBarViewModel(self, repository: factory.makeTabBarRepository())
    }()
    
    required init(factory: TabBarFactory, coordinator: MainCoordinator, defaults: DefaultsManager, controllers: [UIViewController]) {
        self.factory = factory
        self.coordinator = coordinator
        self.defaults = defaults
        self.controllers = controllers
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLayoutSubviews() {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view.
        
        self.configTabBar()
        
    }

    // MARK: - Controller's life cycle methods
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Memory warning!")
    }
    
}

extension TabBarVC {
    
    // MARK: - Config TabBar
    
    private func configTabBar() {

        self.viewControllers = self.controllers
        
        let tabBarAppearance: UITabBarAppearance = UITabBarAppearance()
        tabBarAppearance.configureWithDefaultBackground()
        tabBarAppearance.backgroundColor = UIColor.white
        self.tabBar.tintColor = .systemOrange
        self.tabBar.standardAppearance = tabBarAppearance
        if #available(iOS 15.0, *) {
            self.tabBar.scrollEdgeAppearance = tabBarAppearance
        } else {
            // Fallback on earlier versions
        }

    }
    
}

extension TabBarVC: TabBarViewDelegate {
    
}

extension TabBarVC: TabBarViewProtocol {

    func showErrorAlert(_ message: String?) {
        
    }
    
}
