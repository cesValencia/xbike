//
//  TabBarView.swift
//  XBikeApp
//
//  Created by David Valencia on 10/08/22.
//

import UIKit

protocol TabBarViewDelegate: NSObjectProtocol {

}

class TabBarView: UIView, ViewDelegate {
    
    weak var delegate: TabBarViewDelegate?
    

    override func layoutSubviews() {}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.initComponents()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initComponents() {

    }
    
    func addComponents() {

    }
    
    func setAutolayout() {
        NSLayoutConstraint.activate([

        ])
    }

}

extension TabBarView {

    
}
